import React from 'react';
import Banner from "./Banner";
import {Card} from "@material-ui/core";
// import 'Home.css';

const Home = () => {
    return (
        <div>
            <Banner/>
            <div className="home__section">
                <Card/>
                <Card/>
                <Card/>
            </div>
            <div className="home__section">
                <Card/>
                <Card/>
                <Card/>
            </div>
        </div>
    );
};

export default Home;
